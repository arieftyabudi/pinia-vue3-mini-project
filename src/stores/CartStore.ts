import { defineStore, acceptHMRUpdate } from "pinia";
import { groupBy } from "lodash";
import { useAuthStore } from "@/stores/AuthUser.ts";
import { useLocalStorage } from "@vueuse/core";

export const useStoreCart = defineStore("StoreCart", {
  historyEnabled: true,
  state() {
    return {
      items: useLocalStorage("CartStore:items", []),
    };
  },
  getters: {
    //using arrow functions but instead using this its using state in argument
    count: (state) => state.items.length,
    isEmpty() {
      return this.count === 0;
    },
    grouped() {
      const grouped = groupBy(this.items, (item) => item.name);
      const sorted = Object.keys(grouped).sort();
      let inOrder = {};
      sorted.forEach((key) => (inOrder[key] = grouped[key]));
      return inOrder;
    },
    total() {
      return this.items?.reduce((acc, initialVal) => acc + initialVal.price, 0);
    },
  },
  actions: {
    checkOut() {
      const user = useAuthStore();
      alert(
        `${user.userName} just bough ${this.count} items with total $${this.total}`
      );
    },
    setItem(item, count) {
      this.deleteItem(item.name);
      this.addToCart(count, item);
    },
    deleteItem(itemName) {
      this.items = this.items.filter((item) => item.name !== itemName);
    },
    addToCart(count, product) {
      count = parseInt(count);
      for (let index = 0; index < count; index++) {
        this.items.push({ ...product });
      }
    },
  },
});

if (import.meta.hot) {
  import.meta.hot.accept(acceptHMRUpdate(useStoreCart, import.meta.hot));
}
